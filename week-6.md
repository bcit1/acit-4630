# Cryptography 2

# Readings/Material

[9. PKI](https://www.linkedin.com/learning/comptia-security-plus-sy0-501-cert-prep-6-cryptography/trust-models?u=2097252)
[11. Securing Protocols](https://www.linkedin.com/learning/comptia-security-plus-sy0-501-cert-prep-2-technologies-and-tools/tls-and-ssl?u=2097252)


# Zoom Link

https://bcit.zoom.us/j/91927966224?pwd=aW5Ob1gwcFNUQmlqVG1weVVncElOUT09

Password: `4630`

# Lab

- Todays [lab](https://seedsecuritylabs.org/Labs_16.04/Crypto/Crypto_Hash_Length_Ext/), will guide you through a hash extension attack.

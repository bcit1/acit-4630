# Authentication
Strong Passwords, Salting, Password Cracking, Biometrics, Access Control Models

## Readings for Next Week

- [5. Secure Network Management - 10. Host Security](https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2/firewall-rule-management?u=2097252)

# Zoom Link

https://bcit.zoom.us/j/91927966224?pwd=aW5Ob1gwcFNUQmlqVG1weVVncElOUT09

Password: `4630`

# Lab 

We are following a guide found [here](https://daveeargle.com/security-assignments/labs/lab_password_cracking.html)
# Cryptography 3
HTTPS PKI, SSL/TLS, Certificate Authorities

# Readings/Material

- [Web Security](https://www.linkedin.com/learning/programming-foundations-web-security-2/types-of-credential-attacks?u=2097252)
- No Quiz


# Zoom Link

https://bcit.zoom.us/j/91927966224?pwd=aW5Ob1gwcFNUQmlqVG1weVVncElOUT09

Password: `4630`

# Lab

- Todays [lab](https://seedsecuritylabs.org/Labs_16.04/Crypto/Crypto_PKI/), PKI.
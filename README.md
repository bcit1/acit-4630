
[![pipeline status](https://gitlab.com/bcit1/acit-4630/badges/master/pipeline.svg)](https://gitlab.com/bcit1/acit-4630/-/commits/master)

---

# ACIT 4630

This will be your primary source of information for this course.

---

## Instructor 

My name is Gurjeet Matharu, and I will be your instructor throughout this course.

The best way to contact me is through Discord.

Email is okay as well gmatharu@bcit.ca

## Zoom

We will be meeting via Zoom.  Please see weekly pages on the sidebar for the zoom link.

## Discord

Our primary means of communication will happen in Discord.  
I will be posting weekly learning resources, and will be facilitating conversations.
As a friendly reminder `Attendance & Participation` is worth 10% of your overall grade, it's in your best interest to participate.

### Servers

The servers are below.  Please join the correct one for your class offering.

1. 4630 A Discord Link: https://discord.gg/8MyrVCj
1. 4630 B Discord Link: https://discord.gg/xr5vjem
1. 4630 C Discord Link: https://discord.gg/TQsh3yE

## Miro

Miro is an online whiteboarding tool.

1. 4630 A: https://miro.com/welcomeonboard/q1kRnbuNFObWv2YIXUNgfqszAnkf6qtu1hnoWW27CxfJtv6KiyWRcM8bw6qsRBcU
2. 4630 B: https://miro.com/welcomeonboard/dnl8LnJra5XwW7J0twbOqOu78xfnwXN0hc1Ybpm5gamXR1TMRkfdB5NZVbanba3g
3. 4630 C: https://miro.com/welcomeonboard/9tTzZ5c4fvaoDwT0Ih8ni97RgyZctXceAHksl3wI8Qqqg9DshXTpU9fJFmV1MqYW


## Weekly Readings 

This is a [flipped classroom](https://en.wikipedia.org/wiki/Flipped_classroom).
There will be learning resources provided, and students are expected to come to class having done the weeks readings/learning.

## Assignments

There will be a total of Four assignements.
Please refer to learning hub to see due dates.


## Presentations

Analyze your assigned case study and prepare a presentation A successful presentation will explain the case to your fellow students, and explain the methods used in the attack.
Answer the following questions as part of your presentation

- Goal of the attacker
- Consequence for the attacked
- Assets compromised
- Attack Method
- How the attack could have been prevented
- Try to be as detailed as possible. Research the technical terms that are new to you (don't say "They hacked into the Windows machine". Instead, say "They used a buffer overflow in the LSASS service to remotely execute shell code")
The length of the presentation should be 20 minutes or less

### Topics

Group 3 - Stuxnet
Group 4 - Petya
Group 5 - Sept Marriott Data Breach




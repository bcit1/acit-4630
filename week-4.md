# Network Protocols
TCP/IP Weaknesses, DNS, Routing, nmap/zmap


# Readings/Material

## Pen Testing Essentials
- [2. Pen Testing Tools](https://www.linkedin.com/learning/penetration-testing-essential-training/an-nmap-refresher?u=2097252) 


## CISSP CERT PREP: 
- [Intro to CISSP](https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2/secure-your-network?u=2097252)
- [1. TCP/IP Networking](https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2/secure-your-network?u=2097252)
- [2. Network Security Devices](https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2/secure-your-network?u=2097252) 
- [3. Designing Secure Networks](https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2/public-and-private-addressing?u=2097252)


# Zoom Link

https://bcit.zoom.us/j/91927966224?pwd=aW5Ob1gwcFNUQmlqVG1weVVncElOUT09

Password: `4630`

# Lab

In this lab we will perform several attacks on the TCP protocol, including the SYN flood attack
the TCP reset attack, and the TCP session hijacking attack.

## Lab Environment

[VirtualBox](https://seedsecuritylabs.org/Labs_16.04/Documents/SEEDVM_VirtualBoxManual.pdf) set up properly.

### Network
You will need 3 machines.

1. Attack Machine
2. Victim/Target
3. Observer

### OS
Operating System: Pre-Built SEED images (Ubuntu)

### Tooling


##### Netwox

Netwox consists of a suite of tools, each having a specific number. You can run a command like
following (the parameters depend on which tool you are using). For some of the tool, you have to run it with
the root privilege:

```
$ netwox number [parameters ... ]
```

If you are not sure how to set the parameters, you can look at the manual by issuing `netwox number
--help`. 

##### Wireshark

Wireshark Tool. You also need a good network-traffic sniffer tool for this lab. Although Netwox
comes with a sniffer, you will find that another tool called Wireshark is a much better sniffer tool. Both
Netwox and Wireshark can be downloaded. If you are using our pre-built virtual machine, both tools
are already installed. To sniff all the network traffic, both tools need to be run by the root.
Enabling the ftp and telnet Servers. For this lab, you may need to enable the ftp and telnet
servers. For the sake of security, these services are usually disabled by default. To enable them in our
pre-built Ubuntu virtual machine, you need to run the following commands as the root user:
Start the ftp server

```
$ service vsftpd start
```

Start the telnet server

```
$ service openbsd-inetd start
```






## Tasks

Attack the TCP/IP protocols.

To simplify the “guess” of TCP sequence numbers and source port numbers, we assume that attackers
are on the same physical network as the victims. Therefore, you can use sniffer tools to get that information.

The following is the list of attacks that need to be implemented.


### Task 1 : SYN Flooding Attack

SYN flood is a form of DoS attack in which attackers send many SYN requests to a victim’s TCP port,
but the attackers have no intention to finish the 3-way handshake procedure. Attackers either use spoofed
IP address or do not continue the procedure. Through this attack, attackers can flood the victim’s queue that
is used for half-opened connections, i.e. the connections that has finished SYN, SYN-ACK, but has not yet
gotten a final ACK back. When this queue is full, the victim cannot take any more connection. See below for a visual representation.

![Figure 1 - SYN Flooding Attack](https://onlinelibrary.wiley.com/cms/asset/33374f93-c190-4e98-8b2e-c7dab0ee1490/sec428-fig-0001-m.jpg)

The size of the queue has a system-wide setting. In Linux, we can check the setting using the following
command:

```
$ sysctl -q net.ipv4.tcp_max_syn_backlog
```

We can use command `netstat -na` to check the usage of the queue, i.e., the number of halfopened connection associated with a listening port. The state for such connections is `SYN-RECV`. If the 3-way handshake is finished, the state of the connections will be `ESTABLISHED`.

In this task, you need to demonstrate the SYN flooding attack. You can use the Netwox tool to conduct
the attack, and then use a sniffer tool to capture the attacking packets. While the attack is going on, run
the `netstat -na` command on the victim machine, and compare the result with that before the attack.

Please also describe how you know whether the attack is successful or not.

The corresponding Netwox tool for this task is numbered 76. 
You can also type `netwox 76 --help` to get the help information.

SYN Cookie Countermeasure: If your attack seems unsuccessful, one thing that you can investigate is
whether the SYN cookie mechanism is turned on. SYN cookie is a defense mechanism to counter the SYN
flooding attack. The mechanism will kick in if the machine detects that it is under the SYN flooding attack.
You can use the sysctl command to turn on/off the SYN cookie mechanism:

```
$ sysctl -a | grep cookie (Display the SYN cookie flag)
$ sysctl -w net.ipv4.tcp_syncookies=0 (turn off SYN cookie)
$ sysctl -w net.ipv4.tcp_syncookies=1 (turn on SYN cookie)
```

Please run your attacks with the SYN cookie mechanism on and off, and compare the results. 


### Task 2 : TCP RST Attacks on telnet and ssh Connections

The TCP RST Attack can terminate an established TCP connection between two victims. For example, if
there is an established telnet connection (TCP) between two users A and B, attackers can spoof a RST
packet from A to B, breaking this existing connection. To succeed in this attack, attackers need to correctly
construct the TCP RST packet.

![RST Attack](https://static.packt-cdn.com/products/9781785887819/graphics/B04965_03_20.jpg)

In this task, you need to launch an TCP RST attack to break an existing telnet connection between A
and B. After that, try the same attack on an ssh connection. Please describe your observations. To simplify
the lab, we assume that the attacker and the victim are on the same LAN, i.e., the attacker can observe the
TCP traffic between A and B.

The corresponding Netwox tool for this task is numbered 78. 
You can also type `netwox 78 --help` to get the help information.

### Debrief

Please select a group member to debrief to the rest of your classmates.

- What did you learn?
- Is TCP/IP a secure mechanism?  If it isn't, why?




# Assignment 2 - VPN

Please find instructions [here](https://seedsecuritylabs.org/Labs_16.04/PDF/Firewall_VPN.pdf)

You will be building a VPN to bypass a firewall.

## Instructions

- You will be graded out of 10
- You may work within groups up to but no more than 3 per group.
- Your deliverable is a microsoft word file containing screenshots and your process.

### Rubric:

1. 20 % 
   - Document Presentation
   - Does it have all names, student numbers of all groups
   - Is it well laid out and readable.
   - Does it use figures that are well labeled

2. 40% 
   - Methodology
   - Methodology is well thought out and showcased
   - Contains screenshots of various tasks and coresponding explainations

3. 40%
   - Tasks Correctness
   - Tasks are correct and produced the correc outcome with coresponding evidence
   - Evidence is clear and easily understood






You need to submit a detailed lab report, with screenshots, to describe what you have done and what you
have observed. You also need to provide explanation to the observations that are interesting or surprising.
Please also list the important code snippets followed by explanation. Simply attaching code without any
explanation will not receive credits.

## Important  
For this assignment providing evidence of the VPN is absolutely critical.  Use wireshark.  
If no evidence is provided, with a clear explination **you will recieve no credit for this assignment**
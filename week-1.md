# Threat Models, Vulnerabilities, Attacks, The Security Mindset, Security Controls



## Readings/Learning Material
BCIT students have access to LinkedIn Learning for [free](https://www.bcit.ca/library/linkedin-learning/).  You will be using this resource quite frequently in this course.

- [LinkedIn Learning Chapter 1. Frameworks and Controls](https://www.linkedin.com/learning/cybersecurity-foundations-2/understanding-the-frameworks-standards-and-technology-that-form-what-we-know-as-cybersecurity?u=2097252) 



## Zoom Link

https://bcit.zoom.us/j/95138438239?pwd=WWZKUkVDTG9sWVB6TTZBSXRDOExhZz09

Password: `4630`


## Schedule

| Activity  | Duration   |  
|---|---|
| Introduction  | 20 min  |  
| Security Open Discussion  | 30 min  | 
| Break  | 10 min  |
| Course Outline - Gitbooks | 20 min |
| Housekeeping - Miro & Discord | 30 min  |
| Break | 10 min |
| Lab | 30 min | 

## Lab


Ensure [docker](https://docs.docker.com/get-docker/) has been installed for your system.

Once installed download a the `kalilinux` image and run an interactive shell.
Install metasploit-framework.

```
docker pull kalilinux/kali-rolling
docker run -t -i kalilinux/kali-rolling /bin/bash
apt-get update && apt-get install metasploit-framework
```

Optional:
Refresher with the [Command Line](https://proquestcombo-safaribooksonline-com.eu1.proxy.openathens.net/book/networking/security/9780135305232/part-i-introducing-linux/ch02_html#X2ludGVybmFsX0h0bWxWaWV3P3htbGlkPTk3ODAxMzUzMDUyMzIlMkZjaDAyX2h0bWwmcXVlcnk9)



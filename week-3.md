# OS Security
OS Defense Mechanisms, Software Architecture, Binary Exploitation


# Readings/Material
- [2. Pen Testing Tools](https://www.linkedin.com/learning/penetration-testing-essential-training/an-nmap-refresher?u=2097252) 
- [Chapter 1](https://proquestcombo-safaribooksonline-com.eu1.proxy.openathens.net/book/software-engineering-and-development/9781492043447/1dot-introduction/idm45774082736328_html#X2ludGVybmFsX0h0bWxWaWV3P3htbGlkPTk3ODE0OTIwNDM0NDclMkZjaF9pbnRyb2R1Y3Rpb25faHRtbCZxdWVyeT0=)



## Zoom Link

https://bcit.zoom.us/j/91927966224?pwd=aW5Ob1gwcFNUQmlqVG1weVVncElOUT09

Password: `4630`




## Lab 1

Use `docker-compose` to create a wordpress stack:

https://docs.docker.com/compose/wordpress/

### Steps

#### Set Up

Use the above tutorial to create your `docker-compose`
Take note of your docker-compose file, particularly the environment variables.

Run `docker-compose up` and then navigate to `localhost:8000` to view your wordpress installation. 

If you see wordpress configuration your set up is complete!

#### Explore

Bash into your running wordpress container.  `docker exec -it <container_id> bash`.

Using bash, print off your environment variables.  Hint: [printenv](https://man7.org/linux/man-pages/man1/printenv.1.html)


### Debrief

Questions to Ponder:

- What if I put my compose into source control?  How would I run automatic scripts and deployments?
- What does [GitGaurdian](https://github.com/GitGuardian/APISecurityBestPractices) do
- How/why are environment variables used.  More importantly how do I protect them. Would a `.gitignore` help? See here for ideas: https://docs.docker.com/compose/environment-variables/
- What about [buffer overflow](https://owasp.org/www-community/attacks/Buffer_Overflow_via_Environment_Variables) attacks via environment variables
- What is the difference between `secure_getenv()` and `getenv()`
- In Bash, if we run `export foo=bar`, does it change the environment variable of
the current process? 

Debrief:
- What went well?
- What didn’t go so well?
- What did you learn?


## Lab 2

We will be using [virtualenv](https://virtualenv.pypa.io/en/latest/) to manage our python environments.

In this lab we are going to get a [Django](https://docs.djangoproject.com/en/3.1/intro/tutorial01/) project up and running using a viritual env.


Make sure you're using python3.

```
$ python3 --version
```

Have pip3 installed.


Now you can install virtualenv.

```
$ pip3 install virtualenv
```

Create your virtualenv.
```
$ python3 -m virtualenv <myvenv>
```

Activate your <myvenv>

Linux:
```
$ source myenv/bin/activate
```

Windows:
```
$ myenv\Scripts\activate
```

Finally start your django server:

```
$ pip install django
$ django-admin startproject mysite
$ cd mysite
$ python manage.py runserver 0.0.0.0:8000
```

Navigate to `localhost:8000` to view your start up page.


Navigate to [localhost:8000/admin](localhost:8000/admin)

View the source of the page and look for `csrfmiddlewaretoken`  

What is this token?
What other security measures does Django offer out of the box?


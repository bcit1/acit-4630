# A1 - Setting up your Pen Test Lab

Your goal is to to set up a lab environment using `docker-compose`.

Once set up you will use `kali-linux` and the `msfconsole` to take advantage of insecure services running on `metasploitable2`.

## Set Up

[Install docker compose](https://docs.docker.com/compose/)

Once installed set up your own `docker-compose.yml` file.

It should look something similar to this.

```
version: '3'
services:
  kali:
    build: .
  target:
    image: tleemcjr/metasploitable2:latest
```

Take note of the `kali` service which is using a [Dockerfile](https://docs.docker.com/engine/reference/builder/)

My Kali Dockerfile is starting point (Optional: add additional packages you may want to use. Ex: `net-tools`):

```
FROM kalilinux/kali-rolling
RUN apt-get update -y
RUN apt-get install metasploit-framework -y
CMD bash
```

Due to the nature of these images we will have to open up two terminals.

Kali linux:

`docker-compose run kali bash`

Target:

```
$ docker-compose run target bash
$ /bin/services.sh
```

## msfconsole

Important commands

- `search mysql` : searching for mysql exploits
- `use <exploit>`: activate the exploit
- `show options`: show options for exploit

# Questions

## Part 1

Assuming you don't know anything about the target machine, how do you find the IP and ports of running services on this network.  
  - Hint: `nmap` is extremly helpful tool here.
  - Hint: `ifconfig` can help you gather more information.

1. What command(s) did you use to determine the running services?
2. What are the running services, IP's and Ports?

## Part 2

Now using `msfconsole` search for exploits using the command `search mysql`.  Using the [Auxiliary Module](https://www.offensive-security.com/metasploit-unleashed/auxiliary-module-reference/) `use auxiliary/scanner/mysql/mysql_login` attempt to login to the `mysql` server. 
Once you've gained access to the server, print show the databases: `show databases;`

1. What is the auxiliary module for msf, how did it help you in this lab?
2. What is the output of `show databases;`
3. What is the purpose of a `Dockerfile`.
4. Add the contents of your `Dockerfile`.
5. What is the purpose of a `docker-compose` file.
6. Add the contents of your `docker-compose` file.

## Delivery

You are requried to create a [markdown](https://github.github.com/gfm/#what-is-github-flavored-markdown-) file as your deliverable.

This single markdown should be well thought out and readable.  

For questions part 2: 2,4,6,8 you may want to use [inline code](https://guides.github.com/features/mastering-markdown/#examples)


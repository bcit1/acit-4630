# OS Security
Viruses, Malware, Rootkits, Botnets, Defenses, Vulnerability Classification, System Hardening

# Readings/Material
- [2. Cyber Threats](https://www.linkedin.com/learning/cybersecurity-foundations-2/understanding-the-frameworks-standards-and-technology-that-form-what-we-know-as-cybersecurity?u=2097252) 
- [Chapter 8](http://proquestcombo.safaribooksonline.com.eu1.proxy.openathens.net/book/certification/9781260011807/chapter-8-determining-the-impact-of-incidents/ch8_html?uicode=BCIT)
- [OWASP](https://owasp.org/www-project-top-ten/)


## Zoom Link

https://bcit.zoom.us/j/91927966224?pwd=aW5Ob1gwcFNUQmlqVG1weVVncElOUT09

Password: `4630`

## Schedule

| Activity  | Duration   |  
|---|---|
| Quiz | 10 min |
| Ethics  | 20 min  |  
| Docker Compose  | 20 min  | 
|  Break | 10 min  |
| Groups | 10 min |
| Lab Overview | 10 min  |
| Lab | 30 min |
| Break | 10 min |
| Lab | 30 min |
| Debrief | 20 min | 


## Lab

[Security Ninjas](https://github.com/cniemira/security-ninjas) is a training program developed by the folks out at [OpenDNS](https://www.opendns.com/)

We will be going through the program today.
The learning outcome of this lab is for you to see many different threats.

Please note, that some of the documentation is outdated.  Your task is to work with your team to resolve the issues and continue the challenges.

Do not download the additional tools until you are sure you need them.
Do not spend too long on each assignemnt/task.
You are not expected to know exactly how to solve each issue, google is your friend!
Do not directly reveal the solution, give it an honest shot, skip it and come back to the solution at the end.

At the end of the lab you will debrief to the rest of the class.

1. What went well.
2. What went wrong.
3. What did you learn.
4. What would you do differently next time?


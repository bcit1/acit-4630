# Web Apps
Cookies, Web App Architecture, Web Attacks, XSS, CSRF, SQL Injections, MITM Attacks

## Readings for Next Week
- [4. Secure Coding Practices](https://www.linkedin.com/learning/comptia-security-plus-sy0-601-cert-prep-2-secure-code-design-and-implementation/input-validation?u=2097252)
- [All Chapters: CompTIA Security+ (SY0-501) Cert Prep: 4 Identity and Access Management](https://www.linkedin.com/learning/comptia-security-plus-sy0-501-cert-prep-4-identity-and-access-management/welcome?resume=false&u=2097252)


# Zoom Link

https://bcit.zoom.us/j/91927966224?pwd=aW5Ob1gwcFNUQmlqVG1weVVncElOUT09

Password: `4630`

# Lab 1

Read instructions [here](https://hub.docker.com/r/vulnerables/web-dvwa)

TLDR:
`docker run --rm -it -p 80:80 vulnerables/web-dvwa`

Username: `admin`
Password: `password`


# Lab 2

Once finished the dvwa start this CTF.
[Natas](https://overthewire.org/wargames/natas/)
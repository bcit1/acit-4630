
# WiFi Security
Wireless Threats, Encryption, Authentication, 802.11i, RADIUS, 802.1x

## Readings for Next Week

- [1 - 3 CompTIA Network](https://www.linkedin.com/learning/comptia-network-plus-n10-007-cert-prep-7-wireless-virtual-cloud-and-mobile-networking/introduction-to-802-11)

# Zoom Link

https://bcit.zoom.us/j/91927966224?pwd=aW5Ob1gwcFNUQmlqVG1weVVncElOUT09

Password: `4630`

# Lab 

- Help out the Open Source community: https://github.com/seed-labs/seed-labs/issues/7
- Choose either DNS-in-a-box or DNSSEC and give feedback https://github.com/seed-labs/seed-labs/tree/master/category-network
- If you find an error you may want to submit a merge request

### Mac OSX
Instructions on how to create the PDF.  Use PDFlatex to create the pdf.

```
brew cask install basictex
sudo tlmgr update --self
sudo tlmgr install texliveonfly
sudo texliveonfly <tex_file>
sudo tlmgr install courier
```




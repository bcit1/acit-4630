# Cryptography 
Hashes, Checkums, Block Ciphers

# Readings/Material

- [8. Encryption](https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering/understanding-encryption?u=2097252)
- [9. Symmetric Cryptography](https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering/data-encryption-standard-des?u=2097252)


# Zoom Link

https://bcit.zoom.us/j/91927966224?pwd=aW5Ob1gwcFNUQmlqVG1weVVncElOUT09

Password: `4630`

# Lab

Todays [lab](https://seedsecuritylabs.org/Labs_16.04/PDF/DNS_Local.pdf) focuses on DNS, a topic from week 4.
Please note, you can download the required files from [here](http://www.cis.syr.edu/~wedu/seed/Labs_12.04/Networking/DNS_Local/).
Please do all tasks up to task *3.1*

Bonus - Continue to task 3.2
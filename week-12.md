# Review

## Readings Next Week
- Please review your labs and bring any questions to class.
- Your final quiz will be a preview of your final exam. (multiple choice questions)

## Overvall Review

This class closely resembles the [CompTIA Security+](https://www.comptia.org/certifications/security) curriculum.

I have included the following links for you to review and prep for the final.

- Review all labs we have attempted from the [seed labs website](https://seedsecuritylabs.org/Labs_16.04/). 
- Review frameworks, particularly NIST, [Foundations and Frameworks](https://www.linkedin.com/learning/cybersecurity-foundations-2/understanding-the-frameworks-standards-and-technology-that-form-what-we-know-as-cybersecurity?u=2097252)
- Understand the tools you needed for the labs, example: nmap [Pen Testing](https://www.linkedin.com/learning/penetration-testing-essential-training/welcome?u=2097252)
- Understand the [OWASP Top 10](https://owasp.org/www-project-top-ten/)
  

Security Plus Course, go through it all:

1. [Cert Prep 1](https://www.linkedin.com/learning/comptia-security-plus-sy0-601-cert-prep-1-threats-attacks-and-vulnerabilities/threats-attacks-and-vulnerabilities?u=2097252)
2. [Cert Prep 2](https://www.linkedin.com/learning/comptia-security-plus-sy0-601-cert-prep-2-secure-code-design-and-implementation/input-validation?u=2097252)
3. [Cert Prep 3](https://www.linkedin.com/learning/comptia-security-plus-sy0-601-cert-prep-3-cryptography-design-and-implementation/cryptography-design-and-implementation?u=2097252)
4. [Cert Prep 4](https://www.linkedin.com/learning/comptia-security-plus-sy0-601-cert-prep-4-identity-and-access-management-design-and-implementation/identity-and-access-management?u=2097252)
5. [Cert Prep 5](https://www.linkedin.com/learning/comptia-security-plus-sy0-601-cert-prep-5-physical-security-design-and-implementation/physical-security?u=2097252)
6. [Cert Prep 6](https://www.linkedin.com/learning/comptia-security-plus-sy0-601-cert-prep-6-cloud-security-design-and-implementation/cloud-security?u=2097252)
7. [Cert Prep 7](https://www.linkedin.com/learning/comptia-security-plus-sy0-601-cert-prep-7-endpoint-security-design-and-implementation/endpoint-security?u=2097252)

Note: make sure you complete the above courses and get that linkedin certificate!

Another Security + Text:
- [CompTIA Text](https://learning.oreilly.com/library/view/comptia-security-sy0-601/9780136798767/)

  
# Zoom Link

https://bcit.zoom.us/j/91927966224?pwd=aW5Ob1gwcFNUQmlqVG1weVVncElOUT09

Password: `4630`
